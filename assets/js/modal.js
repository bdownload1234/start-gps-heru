$(document).ready(() =>{
    $('input[id=route-from]').change(() => {
      $('input[id=route-to]').attr('min', $('input[id=route-from]').val());
      $('input[id=route-to]').change(() => {
        $('.route-result').removeClass('d-none');
      });
    });


    $("#sel").select2({
      dropdownParent: $('.search'),
      placeholder: "Meters",
      minimumResultsForSearch: Infinity,
    });


    $('#sel').change(function()
    {
        $('#sel').width( $('#hidnSpan').html($('#sel').find(":selected").text()).width())
    });


    // let form_el = $('form');
    // form_el.on('input change keyup',() => {
    //   form_el.find(':input:focus').each(
    //     (index, el) => {
    //       if(el.value.length > 0) {
    //         $(el).closest('.modal-content').find('.modal-footer button').removeClass('btn-secondary disabled');
    //         $(el).closest('.modal-content').find('.modal-footer button').addClass('btn-primary');
    //       } else {
    //         $(el).closest('.modal-content').find('.modal-footer button').addClass('btn-secondary disabled');
    //         $(el).closest('.modal-content').find('.modal-footer button').removeClass('btn-primary');
    //       }
    //     }
    //   );
    // });

    $(".poi-form-name, .poi-form-contact, .poi-form-phone, .poi-form-description, .poi-form-address, .day-select, .dropdown-search, .selectall").on("input", function() {
      // alert($('.dropdown-search').val());
      poival();
    });

    $(".dropdown-search2").on("change", function() {
      // alert($('.dropdown-search').val());
      poival();
    });

    $("#image-upload").on("change", function() {
      // alert($('.dropdown-search').val());
      poival();
    });
    

    function poival() {
      var can = true;
      $(".poi-form-name, .poi-form-contact, .poi-form-phone, .poi-form-description, .poi-form-address, .day-select, .dropdown-search").each(function() {
        if ($(".poi-form-name").val().length == 0 ||  $(".poi-form-contact").val().length == 0 || $(".poi-form-phone").val().length == 0 || $(".poi-form-description").val().length == 0 || $(".poi-form-address").val().length == 0 || $(".day-select:checked").length == 0 || $('.dropdown-search').val() == '') {
          can = false;
        }
      });
      if (can) {
        $('.poi-submit').css({
          background: '#0073E5'
        });
        $('.poi-submit').bind('click.poistore', function() {poistore();});
        $('.poi-submit').bind('click.poiredirect', function() {window.location.href='index-poi.html';});
      } else {
        $('.poi-submit').css({
          background: '#C2C2C2'
        });
        $('.poi-submit').unbind('click.poistore');
        $('.poi-submit').unbind('click.poiredirect');
      }
    }

    $(".geo-form-name, .geo-form-vehicle, .select-box__input, .day-select-2, .selectall2").on("input", function() {
      // alert($(".day-select-2:checked").length);
      var can = true;
      $(".geo-form-name, .geo-form-vehicle, .select-box__input, .day-select-2").each(function() {
        if ($(".geo-form-name").val().length == 0 ||  $(".geo-form-vehicle").val().length == 0 || $(".select-box__input:checked").val() == '0' || $(".day-select-2:checked").length == 0) {
          can = false;
        }
      });
      if (can) {
        $('.geofence-submit').css({
          background: '#0073E5'
        });
        $('.geofence-submit').bind('click.geostore', function() {geostore();});
        $('.geofence-submit').bind('click.georedirect', function() {window.location.href='index-geofence.html';});
      } else {
        $('.geofence-submit').css({
          background: '#C2C2C2'
        });
        $('.geofence-submit').unbind('click.geostore');
        $('.geofence-submit').unbind('click.georedirect');
      }
    });

    $("input.route-form[type='text']").on("input", function() {
      var can = true;
      $("input.route-form[type='text']").each(function() {
        if ($(this).val().length == 0) {
          can = false;
        }
      });
      if (can) {
        $('.route-submit').css({
          background: '#0073E5'
        });
        $('.route-submit').bind('click.routeredirect', function() {window.location.href='index-route.html';});
      } else {
        $('.route-submit').css({
          background: '#C2C2C2'
        });
        $('.route-submit').unbind('click.routeredirect');
      }
    });


    // warna radius form Route
    $('.custom-radios input[type="radio"]#color-1').click(function() {
      if ($(this).is(':checked')) {
        $('.custom-radios input[type="radio"]#color-1 + label span').css("opacity", "1");
        $('.custom-radios input[type="radio"]#color-2 + label span').css("opacity", "0.5");
        $('.custom-radios input[type="radio"]#color-3 + label span').css("opacity", "0.5");
        $('.custom-radios input[type="radio"]#color-4 + label span').css("opacity", "0.5");
      } else {
        $('.custom-radios input[type="radio"]#color-1 + label span').css("opacity", "1");
        $('.custom-radios input[type="radio"]#color-2 + label span').css("opacity", "1");
        $('.custom-radios input[type="radio"]#color-3 + label span').css("opacity", "1");
        $('.custom-radios input[type="radio"]#color-4 + label span').css("opacity", "1");
      }
    });
    $('.custom-radios input[type="radio"]#color-2').click(function() {
      if ($(this).is(':checked')) {
        $('.custom-radios input[type="radio"]#color-1 + label span').css("opacity", "0.5");
        $('.custom-radios input[type="radio"]#color-2 + label span').css("opacity", "1");
        $('.custom-radios input[type="radio"]#color-3 + label span').css("opacity", "0.5");
        $('.custom-radios input[type="radio"]#color-4 + label span').css("opacity", "0.5");
      } else {
        $('.custom-radios input[type="radio"]#color-1 + label span').css("opacity", "1");
        $('.custom-radios input[type="radio"]#color-2 + label span').css("opacity", "1");
        $('.custom-radios input[type="radio"]#color-3 + label span').css("opacity", "1");
        $('.custom-radios input[type="radio"]#color-4 + label span').css("opacity", "1");
      }
    });
    $('.custom-radios input[type="radio"]#color-3').click(function() {
      if ($(this).is(':checked')) {
        $('.custom-radios input[type="radio"]#color-1 + label span').css("opacity", "0.5");
        $('.custom-radios input[type="radio"]#color-2 + label span').css("opacity", "0.5");
        $('.custom-radios input[type="radio"]#color-3 + label span').css("opacity", "1");
        $('.custom-radios input[type="radio"]#color-4 + label span').css("opacity", "0.5");
      } else {
        $('.custom-radios input[type="radio"]#color-1 + label span').css("opacity", "1");
        $('.custom-radios input[type="radio"]#color-2 + label span').css("opacity", "1");
        $('.custom-radios input[type="radio"]#color-3 + label span').css("opacity", "1");
        $('.custom-radios input[type="radio"]#color-4 + label span').css("opacity", "1");
      }
    });
    $('.custom-radios input[type="radio"]#color-4').click(function() {
      if ($(this).is(':checked')) {
        $('.custom-radios input[type="radio"]#color-1 + label span').css("opacity", "0.5");
        $('.custom-radios input[type="radio"]#color-2 + label span').css("opacity", "0.5");
        $('.custom-radios input[type="radio"]#color-3 + label span').css("opacity", "0.5");
        $('.custom-radios input[type="radio"]#color-4 + label span').css("opacity", "1");
      } else {
        $('.custom-radios input[type="radio"]#color-1 + label span').css("opacity", "1");
        $('.custom-radios input[type="radio"]#color-2 + label span').css("opacity", "1");
        $('.custom-radios input[type="radio"]#color-3 + label span').css("opacity", "1");
        $('.custom-radios input[type="radio"]#color-4 + label span').css("opacity", "1");
      }
    });

    // dropdown category geofence
    $('.select-box__input').click(function() {
      if ($(this).is(':checked')) {
        $('.select-box__current').css("outline", "1px solid #000000");
      } else {
        $('.select-box__current').css("outline", "1px solid #c2c2c2");
      }
    });

    // card route style
    $(".card-route-1").bind('click', function(event){
      $(".card-route-1").css("border", "1.5px solid #404040");
      $(".card-route-1").css("background-color", "#F5F5F5");
      $(".card-route-2").css("border", "1px solid #C2C2C2");
      $(".card-route-2").css("background-color", "");
      $(".card-route-3").css("border", "1px solid #C2C2C2");
      $(".card-route-3").css("background-color", "");
    })
    $(".card-route-2").bind('click', function(event){
      $(".card-route-1").css("border", "1px solid #C2C2C2");
      $(".card-route-1").css("background-color", "");
      $(".card-route-2").css("border", "1.5px solid #404040");
      $(".card-route-2").css("background-color", "#F5F5F5");
      $(".card-route-3").css("border", "1px solid #C2C2C2");
      $(".card-route-3").css("background-color", "");
    })
    $(".card-route-3").bind('click', function(event){
      $(".card-route-1").css("border", "1px solid #C2C2C2");
      $(".card-route-1").css("background-color", "");
      $(".card-route-2").css("border", "1px solid #C2C2C2");
      $(".card-route-2").css("background-color", "");
      $(".card-route-3").css("border", "1.5px solid #404040");
      $(".card-route-3").css("background-color", "#F5F5F5");
    })

    // operational-time select all OPI
    $('.selectall').click(function() {
      if ($(this).is(':checked')) {
        $('.select-day input').prop('checked', true);
        $('.opi-mon').removeClass("border-ops-mon");
        $('.opi-mon').removeClass("opi-mon-validator");
        $('.opi-mon').addClass("opi-mon-checked");
        $('.opi-tue').removeClass("border-ops-tue");
        $('.opi-tue').removeClass("opi-tue-validator");
        $('.opi-tue').addClass("opi-tue-checked");
        $('.opi-wed').removeClass("border-ops-wed");
        $('.opi-wed').removeClass("opi-wed-validator");
        $('.opi-wed').addClass("opi-wed-checked");
        $('.opi-thu').removeClass("border-ops-thu");
        $('.opi-thu').removeClass("opi-thu-validator");
        $('.opi-thu').addClass("opi-thu-checked");
        $('.opi-fri').removeClass("border-ops-fri");
        $('.opi-fri').removeClass("opi-fri-validator");
        $('.opi-fri').addClass("opi-fri-checked");
        $('.opi-sat').removeClass("border-ops-sat");
        $('.opi-sat').removeClass("opi-sat-validator");
        $('.opi-sat').addClass("opi-sat-checked");
        $('.opi-sun').removeClass("border-ops-sun");
        $('.opi-sun').removeClass("opi-sun-validator");
        $('.opi-sun').addClass("opi-sun-checked");
        $('.invalid-opstime').addClass("d-none");
      } else {
        $('.select-day input').prop('checked', false);
        $('.opi-mon').removeClass("opi-mon-checked");
        $('.opi-mon').addClass("border-ops-mon");
        $('.opi-tue').removeClass("opi-tue-checked");
        $('.opi-tue').addClass("border-ops-tue");
        $('.opi-wed').removeClass("opi-wed-checked");
        $('.opi-wed').addClass("border-ops-wed");
        $('.opi-thu').removeClass("opi-thu-checked");
        $('.opi-thu').addClass("border-ops-thu");
        $('.opi-fri').removeClass("opi-fri-checked");
        $('.opi-fri').addClass("border-ops-fri");
        $('.opi-sat').removeClass("opi-sat-checked");
        $('.opi-sat').addClass("border-ops-sat");
        $('.opi-sun').removeClass("opi-sun-checked");
        $('.opi-sun').addClass("border-ops-sun");
      }
    });

    // operational-time select all Geofence
    $('.selectall2').click(function() {
      if ($('.selectall2').is(':checked')) {
        $('.select-day2 input').prop('checked', true);
        $('.geo-mon').removeClass("border-ops-mon");
        $('.geo-mon').removeClass("geo-mon-validator");
        $('.geo-mon').addClass("geo-mon-checked");
        $('.geo-tue').removeClass("border-ops-tue");
        $('.geo-tue').removeClass("geo-tue-validator");
        $('.geo-tue').addClass("geo-tue-checked");
        $('.geo-wed').removeClass("border-ops-wed");
        $('.geo-wed').removeClass("geo-wed-validator");
        $('.geo-wed').addClass("geo-wed-checked");
        $('.geo-thu').removeClass("border-ops-thu");
        $('.geo-thu').removeClass("geo-thu-validator");
        $('.geo-thu').addClass("geo-thu-checked");
        $('.geo-fri').removeClass("border-ops-fri");
        $('.geo-fri').removeClass("geo-fri-validator");
        $('.geo-fri').addClass("geo-fri-checked");
        $('.geo-sat').removeClass("border-ops-sat");
        $('.geo-sat').removeClass("geo-sat-validator");
        $('.geo-sat').addClass("geo-sat-checked");
        $('.geo-sun').removeClass("border-ops-sun");
        $('.geo-sun').removeClass("geo-sun-validator");
        $('.geo-sun').addClass("geo-sun-checked");
        $('.invalid-opstime-geo').addClass("d-none");
      } else {
        $('.select-day2 input').prop('checked', false);
        $('.geo-mon').removeClass("geo-mon-checked");
        $('.geo-mon').addClass("border-ops-mon");
        $('.geo-tue').removeClass("geo-tue-checked");
        $('.geo-tue').addClass("border-ops-tue");
        $('.geo-wed').removeClass("geo-wed-checked");
        $('.geo-wed').addClass("border-ops-wed");
        $('.geo-thu').removeClass("geo-thu-checked");
        $('.geo-thu').addClass("border-ops-thu");
        $('.geo-fri').removeClass("geo-fri-checked");
        $('.geo-fri').addClass("border-ops-fri");
        $('.geo-sat').removeClass("geo-sat-checked");
        $('.geo-sat').addClass("border-ops-sat");
        $('.geo-sun').removeClass("geo-sun-checked");
        $('.geo-sun').addClass("border-ops-sun");
      }
    });

    // operational time day select Geofence
    $('.day-select-2').click(function() {
      if ($('.day-select-2').is(':checked')) {
        $('.geo-mon').removeClass("border-ops-mon");
        $('.geo-mon').removeClass("geo-mon-validator");
        $('.geo-mon').addClass("geo-mon-checked");
        $('.geo-tue').removeClass("border-ops-tue");
        $('.geo-tue').removeClass("geo-tue-validator");
        $('.geo-tue').addClass("geo-tue-checked");
        $('.geo-wed').removeClass("border-ops-wed");
        $('.geo-wed').removeClass("geo-wed-validator");
        $('.geo-wed').addClass("geo-wed-checked");
        $('.geo-thu').removeClass("border-ops-thu");
        $('.geo-thu').removeClass("geo-thu-validator");
        $('.geo-thu').addClass("geo-thu-checked");
        $('.geo-fri').removeClass("border-ops-fri");
        $('.geo-fri').removeClass("geo-fri-validator");
        $('.geo-fri').addClass("geo-fri-checked");
        $('.geo-sat').removeClass("border-ops-sat");
        $('.geo-sat').removeClass("geo-sat-validator");
        $('.geo-sat').addClass("geo-sat-checked");
        $('.geo-sun').removeClass("border-ops-sun");
        $('.geo-sun').removeClass("geo-sun-validator");
        $('.geo-sun').addClass("geo-sun-checked");
        $('.invalid-opstime-geo').addClass("d-none");
      } else {
        $('.geo-mon').removeClass("geo-mon-checked");
        $('.geo-mon').addClass("border-ops-mon");
        $('.geo-tue').removeClass("geo-tue-checked");
        $('.geo-tue').addClass("border-ops-tue");
        $('.geo-wed').removeClass("geo-wed-checked");
        $('.geo-wed').addClass("border-ops-wed");
        $('.geo-thu').removeClass("geo-thu-checked");
        $('.geo-thu').addClass("border-ops-thu");
        $('.geo-fri').removeClass("geo-fri-checked");
        $('.geo-fri').addClass("border-ops-fri");
        $('.geo-sat').removeClass("geo-sat-checked");
        $('.geo-sat').addClass("border-ops-sat");
        $('.geo-sun').removeClass("geo-sun-checked");
        $('.geo-sun').addClass("border-ops-sun");
      }
    });

    

    // operational time day select OPI
    $('.day-select').click(function() {
      if ($('.day-select').is(':checked')) {
        $('.opi-mon').removeClass("border-ops-mon");
        $('.opi-mon').removeClass("opi-mon-validator");
        $('.opi-mon').addClass("opi-mon-checked");
        $('.opi-tue').removeClass("border-ops-tue");
        $('.opi-tue').removeClass("opi-tue-validator");
        $('.opi-tue').addClass("opi-tue-checked");
        $('.opi-wed').removeClass("border-ops-wed");
        $('.opi-wed').removeClass("opi-wed-validator");
        $('.opi-wed').addClass("opi-wed-checked");
        $('.opi-thu').removeClass("border-ops-thu");
        $('.opi-thu').removeClass("opi-thu-validator");
        $('.opi-thu').addClass("opi-thu-checked");
        $('.opi-fri').removeClass("border-ops-fri");
        $('.opi-fri').removeClass("opi-fri-validator");
        $('.opi-fri').addClass("opi-fri-checked");
        $('.opi-sat').removeClass("border-ops-sat");
        $('.opi-sat').removeClass("opi-sat-validator");
        $('.opi-sat').addClass("opi-sat-checked");
        $('.opi-sun').removeClass("border-ops-sun");
        $('.opi-sun').removeClass("opi-sun-validator");
        $('.opi-sun').addClass("opi-sun-checked");
        $('.invalid-opstime').addClass("d-none");
      } else {
        $('.opi-mon').removeClass("opi-mon-checked");
        $('.opi-mon').addClass("border-ops-mon");
        $('.opi-tue').removeClass("opi-tue-checked");
        $('.opi-tue').addClass("border-ops-tue");
        $('.opi-wed').removeClass("opi-wed-checked");
        $('.opi-wed').addClass("border-ops-wed");
        $('.opi-thu').removeClass("opi-thu-checked");
        $('.opi-thu').addClass("border-ops-thu");
        $('.opi-fri').removeClass("opi-fri-checked");
        $('.opi-fri').addClass("border-ops-fri");
        $('.opi-sat').removeClass("opi-sat-checked");
        $('.opi-sat').addClass("border-ops-sat");
        $('.opi-sun').removeClass("opi-sun-checked");
        $('.opi-sun').addClass("border-ops-sun");
      }
    });

    $('.dropdown-search').selectize({
      searchField: 'title',
      valueField: 'value',
      placeholder: 'Placeholder',
      options: [
          {value: 1, title: 'Pool', icon: 'fa-circle'},
          {value: 2, title: 'Shop', icon: 'fa-circle'},
          {value: 3, title: 'Workshop', icon: 'fa-circle'},
          {value: 4, title: 'Garage', icon: 'fa-circle'},
          {value: 5, title: 'Resto', icon: 'fa-circle'},
          {value: 6, title: 'Warehouse', icon: 'fa-circle'},
          {value: 7, title: 'Factory', icon: 'fa-circle'}
      ],
      render: {
          option: function(data, escape) {
              return '<div class="option">' +
                      '<i class="fas fa-fw ' + escape(data.icon) + ' text-secondary font-12 me-2"></i>' +
                      '<span class="title font-14">' + escape(data.title) + '</span>' +
                      '</div>';
          },
          item: function(data, escape) {
              return '<div class="item"><i class="fas fa-fw '  + escape(data.icon) + ' text-secondary font-12 me-1"></i>' + escape(data.title) + '</div>';
          }
      }
    });

    $('.dropdown-search2').selectize({
      searchField: 'title',
      valueField: 'value',
      placeholder: 'Placeholder',
      options: [
          {value: 1, title: 'Pool', icon: 'fa-circle'},
          {value: 2, title: 'Shop', icon: 'fa-circle'},
          {value: 3, title: 'Workshop', icon: 'fa-circle'},
          {value: 4, title: 'Garage', icon: 'fa-circle'},
          {value: 5, title: 'Resto', icon: 'fa-circle'},
          {value: 6, title: 'Warehouse', icon: 'fa-circle'},
          {value: 7, title: 'Factory', icon: 'fa-circle'}
      ],
      items: [sessionStorage.getItem("dropdown-search2") != null ? sessionStorage.getItem("dropdown-search2"):sessionStorage.getItem("dropdown-search") ],
      render: {
          option: function(data, escape) {
              return '<div class="option">' +
                      '<i class="fas fa-fw ' + escape(data.icon) + ' text-secondary font-12 me-2"></i>' +
                      '<span class="title font-14">' + escape(data.title) + '</span>' +
                      '</div>';
          },
          item: function(data, escape) {
              return '<div class="item"><i class="fas fa-fw '  + escape(data.icon) + ' text-secondary font-12 me-1"></i>' + escape(data.title) + '</div>';
          }
      }
    });
  });


  

  $(".dropdown-search" ).change(function() {
    if ($(this).val() != '') {
        $('.items').removeClass("selectize-input-invalid");
        $('.invalid-selectize').addClass("d-none");
      }
  });

  $(".select-box__input" ).change(function() {
    if ($(this).val() != '0') {
      $('.select-box__current').addClass("select-box__current-valid");
      $('.select-box__current').removeClass("select-box__current-invalid");
      $('.invalid-select-box-geo').addClass("d-none");
      }
  });


  $(".menulink").on("click", function() {
    if($(".circle").css("background") === "#0073E5") {
      $(".circle").css("background", "#c4c4c4");
    } else{
      $(".circle").css("background", "#0073E5");
    }
  });

  $('#image-upload').on('change', function(e) {
    $('#image-upload-preview').attr('src', URL.createObjectURL(e.target.files[0]));
    var reader = new FileReader();
    reader.readAsDataURL(e.target.files[0]); 
    reader.onloadend = function() {
      var base64data = reader.result;
      sessionStorage.setItem("image-upload", base64data)             
      console.log(base64data);
    }
    $('#image-upload-zone').addClass('uploaded');
  });

  $('#image-upload-zone').on({
    'drop': function(evt) {
      evt.preventDefault();
      var dT = evt.originalEvent.dataTransfer;
      var files = dT.files;
      if (files && files.length) {
        $('#image-upload')[0].files = files;
        $('#image-upload-preview').attr('src', URL.createObjectURL(files[0]));
        var reader = new FileReader();
        reader.readAsDataURL(files[0]); 
        reader.onloadend = function() {
          var base64data = reader.result;
          sessionStorage.setItem("image-upload", base64data)             
          console.log(base64data);
        }
        $('#image-upload-zone').addClass('uploaded');
      }
    },
    'dragenter': function(e) { e.preventDefault(); },
    'dragover': function(e) {
      e.preventDefault();
    }
  });

  (function () {
    'use strict'

    // Fetch all the forms we want to apply custom Bootstrap validation styles to
    var forms = document.querySelectorAll('.needs-validation')

    // Loop over them and prevent submission
    Array.prototype.slice.call(forms)
      .forEach(function (form) {
        form.addEventListener('submit', function (event) {
          if (!form.checkValidity() || ($('.day-select').not(':checked')) || ($('.day-select2').not(':checked')) ) {
            event.preventDefault()
            event.stopPropagation()
          }

          form.classList.add('was-validated')
        }, false)
      })
  })()

  // function check operational time validation when submit POI
  $('.poi-submit').click(function() {
      if ($('.day-select:checked').length == 0) {
        $('.opi-mon').removeClass("border-ops-mon");
        $('.opi-mon').addClass("opi-mon-validator");
        $('.opi-tue').removeClass("border-ops-tue");
        $('.opi-tue').addClass("opi-tue-validator");
        $('.opi-wed').removeClass("border-ops-wed");
        $('.opi-wed').addClass("opi-wed-validator");
        $('.opi-thu').removeClass("border-ops-thu");
        $('.opi-thu').addClass("opi-thu-validator");
        $('.opi-fri').removeClass("border-ops-fri");
        $('.opi-fri').addClass("opi-fri-validator");
        $('.opi-sat').removeClass("border-ops-sat");
        $('.opi-sat').addClass("opi-sat-validator");
        $('.opi-sun').removeClass("border-ops-sun");
        $('.opi-sun').addClass("opi-sun-validator");
        $('.invalid-opstime').removeClass("d-none");
      } else {
        $('.opi-mon').removeClass("opi-mon-validator");
        $('.opi-mon').addClass("border-ops-mon");
        $('.opi-tue').removeClass("opi-tue-validator");
        $('.opi-tue').addClass("border-ops-tue");
        $('.opi-wed').removeClass("opi-wed-validator");
        $('.opi-wed').addClass("border-ops-wed");
        $('.opi-thu').removeClass("opi-thu-validator");
        $('.opi-thu').addClass("border-ops-thu");
        $('.opi-fri').removeClass("opi-fri-validator");
        $('.opi-fri').addClass("border-ops-fri");
        $('.opi-sat').removeClass("opi-sat-validator");
        $('.opi-sat').addClass("border-ops-sat");
        $('.opi-sun').removeClass("opi-sun-validator");
        $('.opi-sun').addClass("border-ops-sun");
        $('.invalid-opstime').addClass("d-none");
      }

      if ($('.dropdown-search').val() == '') {
        $('.items').addClass("selectize-input-invalid");
        $('.invalid-selectize').removeClass("d-none");
      } else{
        $('.items').removeClass("selectize-input-invalid");
        $('.invalid-selectize').addClass("d-none");
      }
  });

  $('.geofence-submit').click(function() {
      if ($('.day-select-2:checked').length == 0) {
        $('.geo-mon').removeClass("border-ops-mon");
        $('.geo-mon').addClass("geo-mon-validator");
        $('.geo-tue').removeClass("border-ops-tue");
        $('.geo-tue').addClass("geo-tue-validator");
        $('.geo-wed').removeClass("border-ops-wed");
        $('.geo-wed').addClass("geo-wed-validator");
        $('.geo-thu').removeClass("border-ops-thu");
        $('.geo-thu').addClass("geo-thu-validator");
        $('.geo-fri').removeClass("border-ops-fri");
        $('.geo-fri').addClass("geo-fri-validator");
        $('.geo-sat').removeClass("border-ops-sat");
        $('.geo-sat').addClass("geo-sat-validator");
        $('.geo-sun').removeClass("border-ops-sun");
        $('.geo-sun').addClass("geo-sun-validator");
        $('.invalid-opstime-geo').removeClass("d-none");
      } else {
        $('.geo-mon').removeClass("geo-mon-validator");
        $('.geo-mon').addClass("border-ops-mon");
        $('.geo-tue').removeClass("geo-tue-validator");
        $('.geo-tue').addClass("border-ops-tue");
        $('.geo-wed').removeClass("geo-wed-validator");
        $('.geo-wed').addClass("border-ops-wed");
        $('.geo-thu').removeClass("geo-thu-validator");
        $('.geo-thu').addClass("border-ops-thu");
        $('.geo-fri').removeClass("geo-fri-validator");
        $('.geo-fri').addClass("border-ops-fri");
        $('.geo-sat').removeClass("geo-sat-validator");
        $('.geo-sat').addClass("border-ops-sat");
        $('.geo-sun').removeClass("geo-sun-validator");
        $('.geo-sun').addClass("border-ops-sun");
        $('.invalid-opstime').addClass("d-none");
      }

      if ($('.select-box__input:checked').val() == '0') {
        $('.select-box__current').removeClass("select-box__current-valid");
        $('.select-box__current').addClass("select-box__current-invalid");
        $('.invalid-select-box-geo').removeClass("d-none");
      } else{
        $('.select-box__current').addClass("select-box__current-valid");
        $('.select-box__current').removeClass("select-box__current-invalid");
        $('.invalid-select-box-geo').addClass("d-none");
      }
  });

  function geostore(){
    var geoName = document.getElementById("geo-name");
    var geoVehicle = document.getElementById("geo-vehicle");
    var opsgeomon = document.getElementById("btn-check8");
    var opsgeotue = document.getElementById("btn-check9");
    var opsgeowed = document.getElementById("btn-check10");
    var opsgeothu = document.getElementById("btn-check11");
    var opsgeofri = document.getElementById("btn-check12");
    var opsgeosat = document.getElementById("btn-check13");
    var opsgeosun = document.getElementById("btn-check14");
    var timeopsgeomon = document.getElementById("time-geo-mon");
    var timeopsgeotue = document.getElementById("time-geo-tue");
    var timeopsgeowed = document.getElementById("time-geo-wed");
    var timeopsgeothu = document.getElementById("time-geo-thu");
    var timeopsgeofri = document.getElementById("time-geo-fri");
    var timeopsgeosat = document.getElementById("time-geo-sat");
    var timeopsgeosun = document.getElementById("time-geo-sun");
    var geostatus = document.getElementById("switchGeofenceStatus");
    var geoeveryday = document.getElementById("switchGeofenceEveryDay");
    var alertonexiting = document.getElementById("alertonexiting");
    var alertonentering = document.getElementById("alertonentering");

    sessionStorage.setItem("geo-name", geoName.value);
    sessionStorage.setItem("geo-vehicle", geoVehicle.value);
    sessionStorage.setItem("btn-check8", opsgeomon.checked);
    sessionStorage.setItem("btn-check9", opsgeotue.checked);
    sessionStorage.setItem("btn-check10", opsgeowed.checked);
    sessionStorage.setItem("btn-check11", opsgeothu.checked);
    sessionStorage.setItem("btn-check12", opsgeofri.checked);
    sessionStorage.setItem("btn-check13", opsgeosat.checked);
    sessionStorage.setItem("btn-check14", opsgeosun.checked);
    sessionStorage.setItem("time-geo-mon", timeopsgeomon.value);
    sessionStorage.setItem("time-geo-tue", timeopsgeotue.value);
    sessionStorage.setItem("time-geo-wed", timeopsgeowed.value);
    sessionStorage.setItem("time-geo-thu", timeopsgeothu.value);
    sessionStorage.setItem("time-geo-fri", timeopsgeofri.value);
    sessionStorage.setItem("time-geo-sat", timeopsgeosat.value);
    sessionStorage.setItem("time-geo-sun", timeopsgeosun.value);
    sessionStorage.setItem("switchGeofenceStatus", geostatus.checked);
    sessionStorage.setItem("switchGeofenceEveryDay", geoeveryday.checked);
    sessionStorage.setItem("alertonexiting", alertonexiting.checked);
    sessionStorage.setItem("alertonentering", alertonentering.checked);
  }

  function poistore(){
    var poiName = document.getElementById("poi-name");
    var poiDescription = document.getElementById("poi-description");
    var poiAdress = document.getElementById("poi-address");
    var poiContact = document.getElementById("poi-contact");
    var poiPhone = document.getElementById("poi-phone");
    var poiCategory = document.getElementById("dropdown-search");
    var poiCategory2 = document.getElementById("dropdown-search2");
    var opspoimon = document.getElementById("btn-check1");
    var opspoitue = document.getElementById("btn-check2");
    var opspoiwed = document.getElementById("btn-check3");
    var opspoithu = document.getElementById("btn-check4");
    var opspoifri = document.getElementById("btn-check5");
    var opspoisat = document.getElementById("btn-check6");
    var opspoisun = document.getElementById("btn-check7");
    var timeopspoimon = document.getElementById("time-ops-mon");
    var timeopspoitue = document.getElementById("time-ops-tue");
    var timeopspoiwed = document.getElementById("time-ops-wed");
    var timeopspoithu = document.getElementById("time-ops-thu");
    var timeopspoifri = document.getElementById("time-ops-fri");
    var timeopspoisat = document.getElementById("time-ops-sat");
    var timeopspoisun = document.getElementById("time-ops-sun");
    var poistatus = document.getElementById("switchPOIStatus");
    var poieveryday = document.getElementById("switchPOIEveryDay");

    sessionStorage.setItem("poi-name", poiName.value);
    sessionStorage.setItem("poi-description", poiDescription.value);
    sessionStorage.setItem("poi-address", poiAdress.value);
    sessionStorage.setItem("poi-contact", poiContact.value);
    sessionStorage.setItem("poi-phone", poiPhone.value);
    if (poiCategory !== null) {
      sessionStorage.setItem("dropdown-search", poiCategory.value);
    } else{
      sessionStorage.setItem("dropdown-search2", poiCategory2.value);
    }
    sessionStorage.setItem("btn-check1", opspoimon.checked);
    sessionStorage.setItem("btn-check2", opspoitue.checked);
    sessionStorage.setItem("btn-check3", opspoiwed.checked);
    sessionStorage.setItem("btn-check4", opspoithu.checked);
    sessionStorage.setItem("btn-check5", opspoifri.checked);
    sessionStorage.setItem("btn-check6", opspoisat.checked);
    sessionStorage.setItem("btn-check7", opspoisun.checked);
    sessionStorage.setItem("time-ops-mon", timeopspoimon.value);
    sessionStorage.setItem("time-ops-tue", timeopspoitue.value);
    sessionStorage.setItem("time-ops-wed", timeopspoiwed.value);
    sessionStorage.setItem("time-ops-thu", timeopspoithu.value);
    sessionStorage.setItem("time-ops-fri", timeopspoifri.value);
    sessionStorage.setItem("time-ops-sat", timeopspoisat.value);
    sessionStorage.setItem("time-ops-sun", timeopspoisun.value);
    sessionStorage.setItem("switchPOIStatus", poistatus.checked);
    sessionStorage.setItem("switchPOIEveryDay", poieveryday.checked);
  }