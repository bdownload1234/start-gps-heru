// dropdown
const targetDiv = document.getElementById("myDropdown");
const btn = document.getElementById("dropdown-btn");

function myFunction() {
    if (targetDiv.style.display !== "none") {
        targetDiv.style.display = "none";
        document.getElementById("dropdown-btn").classList.toggle("dropbtn-active");
    } else {
    targetDiv.style.display = "block";
    btn.classList.remove('dropbtn-active');
    }
};

// Changes color on hover
$(function () {
    $('a.btn-create').hover(function () {
    $('.icon-bottom-create').addClass('d-none');
    $('.icon-bottom-create-focus').removeClass('d-none');
    },
    function () {
        $('.icon-bottom-create-focus').addClass('d-none');
        $('.icon-bottom-create').removeClass('d-none');
    });
});

$(".dropdown-fleet-type").selectize({
  searchField: "title",
  valueField: "value",
  placeholder: "Placeholder",
  maxItems: 3,
  items: [1, 2],
  options: [
    { value: 1, title: "Truck" },
    { value: 2, title: "Dump Truck" },
    { value: 3, title: "Fleet Type" },
    { value: 4, title: "Fleet Type" },
    { value: 5, title: "Fleet Type" },
    { value: 6, title: "Fleet Type" },
    { value: 7, title: "Fleet Type" },
    { value: 8, title: "Fleet Type" },
  ],
  render: {
    option: function (data, escape) {
      return (
        '<div class="option">' +
        // '<i class="fas fa-fw fa-square-o text-secondary font-12 me-2"></i>' +
        '<span class="title font-14">' +
        escape(data.title) +
        "</span>" +
        "</div>"
      );
    },
    item: function (data, escape) {
      return (
        '<div class="item">' +
        '<img src="./assets/img/misc/box-tagging-fleet.png" class="img-box-fleet">' +
        escape(data.title) +
        '<img src="./assets/img/misc/elipse-tagging-fleet.png" class="img-elipse-fleet"></div>'
      );
    },
  },
});

$(".dropdown-fleet-info").selectize({
    searchField: "title",
    valueField: "value",
    placeholder: "Placeholder",
    maxItems: 8,
    items: [1, 2],
    options: [
      { value: 1, title: "Truck" },
      { value: 2, title: "Dump Truck" },
      { value: 3, title: "Fleet Type" },
      { value: 4, title: "Fleet Type" },
      { value: 5, title: "Fleet Type" },
      { value: 6, title: "Fleet Type" },
      { value: 7, title: "Fleet Type" },
      { value: 8, title: "Fleet Type" },
    ],
    render: {
      option: function (data, escape) {
        return (
          '<div class="option">' +
          // '<i class="fas fa-fw fa-square-o text-secondary font-12 me-2"></i>' +
          '<span class="title font-14">' +
          escape(data.title) +
          "</span>" +
          "</div>"
        );
      },
      item: function (data, escape) {
        return (
          '<div class="item">' +
          '<img src="./assets/img/misc/box-tagging-fleet.png" class="img-box-fleet">' +
          escape(data.title) +
          '<img src="./assets/img/misc/elipse-tagging-fleet.png" class="img-elipse-fleet"></div>'
        );
      },
    },
  });
  