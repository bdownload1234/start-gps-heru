$(document).ready(() => {

    $('#close').on('click', () => {
        $('.success-close-order').addClass('d-none');
    })
  
    $('#save').on('click', () => {
        $('.success-close-order').removeClass('d-none');
    })

    $('.close-sc-co').on('click', () => {
        $('.success-close-order').addClass('d-none');
    })

    // function head title

    $(".vehicle").click(function () {
        $('.name-doc').addClass('d-none');
        $('.name-doc-vehicle').removeClass('d-none');        
    });

    $(".fleet").click(function () {
        $('.name-doc').removeClass('d-none');
        $('.name-doc-vehicle').addClass('d-none');        
    });

    $('.user-info-dropdown').selectize({
        searchField: 'title',
        valueField: 'value',
        placeholder: 'Pilih satu',
        items : [1],
        options: [
            {value: 1, title: 'Username - Nama Lengkap'},
            {value: 2, title: 'Username - Nama Lengkap'},
            {value: 3, title: 'Username - Nama Lengkap'},
            {value: 4, title: 'Username - Nama Lengkap'}
        ],
        render: {
            option: function(data, escape) {
                return '<div class="option"' +
                        '<span class="title font-14">' + escape(data.title) + '</span>' +
                        '</div>';
            },
            item: function(data, escape) {
                return '<div class="item">' + escape(data.title) + '</div>';
            }
        }
    });

    $('#event-from').datepicker({
        dayNamesMin: ['S', 'M', 'T', 'W', 'T', 'F', 'S'],
        dateFormat: "dd MM yy"
    });

    $('.wo-install-dropdown').selectize({
        searchField: 'title',
        valueField: 'value',
        placeholder: 'Pilih satu',
        items : [1],
        options: [
            {value: 1, title: 'Username - Nama Lengkap'},
            {value: 2, title: 'Username - Nama Lengkap'},
            {value: 3, title: 'Username - Nama Lengkap'},
            {value: 4, title: 'Username - Nama Lengkap'}
        ],
        render: {
            option: function(data, escape) {
                return '<div class="option"' +
                        '<span class="title font-14">' + escape(data.title) + '</span>' +
                        '</div>';
            },
            item: function(data, escape) {
                return '<div class="item">' + escape(data.title) + '</div>';
            }
        }
    });

    $('#event-from').datepicker({
        dayNamesMin: ['S', 'M', 'T', 'W', 'T', 'F', 'S'],
        dateFormat: "dd MM yy"
    });

    $('#timepicker').timepicker({
        timeFormat: 'h:mm p',
        // interval: 60,
        // minTime: '10',
        // maxTime: '6:00pm',
        // startTime: '10:00',
        dynamic: true,
        dropdown: true,
        scrollbar: true
    });

    // dropdown
    const targetDiv = document.getElementById("myDropdown");
    const btn = document.getElementById("dropdown-btn");

    function myFunction() {
        if (targetDiv.style.display !== "none") {
            targetDiv.style.display = "none";
            document.getElementById("dropdown-btn").classList.toggle("dropbtn-active");
        } else {
        targetDiv.style.display = "block";
        btn.classList.remove('dropbtn-active');
        }
    };
    
});