var countChecked = function($table, checkboxClass) {
    if ($table) {
        // Find all elements with given class
        var chkAll = $table.find(checkboxClass);
        // Count checked checkboxes
        var checked = chkAll.filter(':checked').length;
        // Count total
        var total = chkAll.length;    
        // Return an object with total and checked values
        return {
            total: total,
            checked: checked
        }
    }
}

// table created
var tableCreated = $('table.table-created').DataTable({
"scrollCollapse": true,
"autoWidth": false,
'columnDefs': [{
    width:"24px",
    'className': 'dt-body-center',
    'targets': 0,
    'searchable':false,
    'orderable':false,
    'render': function (data, type, full, meta){
        return '<input type="checkbox" class="created-check-element" value="' 
            + $('<div/>').text(data).html() + '">';
    }
},
],
'order': [1, 'asc'],
"paging": true,
"lengthChange": true,
"showNEntries": true,
"searching": true,
"bInfo": true,
// "bPaginate": false,
"language": {
"lengthMenu": ``,
"info": `_TOTAL_ Data <div class="line-data"></div>`,
"sInfoFiltered": "",
"search": "_INPUT_",
"searchPlaceholder": "Cari data",
"emptyTable": `
    <img src="assets/img/misc/no-data-table.svg" class="d-block mx-auto mb-3">
    <label>Belum ada data yang tersedia, <a class="text-link">Create Data</a></label>
    `,
},
infoCallback: function( settings, start, end, max, total, pre ) {
    if (total == 0) return ''+total+' Data <div class="line-data"></div>';
    return ''+total+' Data <div class="line-data"></div>';
}
});
      
$(document).on('change', '.created-check-element',  function() {
    var result = countChecked($('.table-created'), '.created-check-element');
    if (result.checked != 0) {
        $('#table-wrapper-created > #measurement-table_wrapper > :nth-child(3) .dataTables_info').addClass('d-none');
        $('#table-wrapper-created > #measurement-table_wrapper > div:nth-child(1) .dataTables_length').addClass('d-none');
        $('#checked').removeClass('d-none');
        $('#table-wrapper-created > #measurement-table_wrapper > div:nth-child(2) table').addClass('mt-54');
        $('#checked').css({
            "position": 'absolute',
            "top": '176px'
        });
        $('#checked').html(result.checked + ' Terseleksi <div class="line-data"></div>'
                            + `<button type="button" class="btn btn-rmv-checked" data-bs-toggle="modal" data-bs-target="#delete-modal-table" style= "margin-top: -6px; padding: 6px;">
                            <img src="assets/img/misc/remove-wo.png" alt="" style="margin-bottom: 2px;" class="icon-remove-checked" width= "14.4px" height="14.4px">
                            <img src="assets/img/misc/remove-wo-focus.png" alt="" style="margin-bottom: 2px;" class="icon-remove-checked-focus d-none" width= "14.4px" height="14.4px">
                                Cancel
                            </button>`);
    } else {
        $('#table-wrapper-created > #measurement-table_wrapper > div:nth-child(2) table').removeClass('mt-54');
        $('#checked').addClass('d-none');
        $('#table-wrapper-created > #measurement-table_wrapper > :nth-child(3) .dataTables_info').removeClass('d-none');
        $('#table-wrapper-created > #measurement-table_wrapper > div:nth-child(1) .dataTables_length').removeClass('d-none');
    }
    // $('#total').html(result.total);
});

// Handle click on "Select all" control
$('#created-select-all').on('click', function(){
    // Check/uncheck all checkboxes in the table
    var rows = tableCreated.rows({ 'search': 'applied' }).nodes();
    $('input[type="checkbox"]', rows).prop('checked', this.checked);
});

// Handle click on checkbox to set state of "Select all" control
$('table.table-created tbody').on('change', 'input[type="checkbox"]', function(){
    // If checkbox is not checked
    if(!this.checked){
        var el = $('#created-select-all').get(0);
        // If "Select all" control is checked and has 'indeterminate' property
        if(el && el.checked && ('indeterminate' in el)){
            // Set visual state of "Select all" control 
            // as 'indeterminate'
            el.indeterminate = true;
        }
    }
});

$('#measurement-table_filter label').addClass('p-relative');
$('#measurement-table_filter label input').css({ 'padding-left': '2.2rem' });
$('#measurement-table_filter label').append(`
    <img class="fas fa-fw fa-search p-absolute text-secondary" style="left: 21px; top: 11px; display: flex; align-items: center; width: 15px; height: 15px;" src= "assets/img/misc/search-icon2.png"></img>
    `);

$('#measurement-table_length').append(`
    <div class="filter-title">Filter</div>
    <select id="dropdown-search" class="form-control shadow-none filter-table selectpicker font-14" required></select>
`);

$('.filter-table').selectize({
    searchField: 'title',
    valueField: 'value',
    placeholder: 'Pilih satu',
    items : [1],
    options: [
        {value: 1, title: 'Option 1'},
        {value: 2, title: 'Option 2'},
        {value: 3, title: 'Option 3'},
        {value: 4, title: 'Option 4'}
    ],
    render: {
        option: function(data, escape) {
            return '<div class="option"' +
                    '<span class="title font-14">' + escape(data.title) + '</span>' +
                    '</div>';
        },
        item: function(data, escape) {
            return '<div class="item">' + escape(data.title) + '</div>';
        }
    }
});


// Changes color on hover
$(function () {
    $('button.btn-remove').hover(function () {
    $('.icon-remove-wo').addClass('d-none');
    $('.icon-remove-wo-focus').removeClass('d-none');
    },
    function () {
        $('.icon-remove-wo-focus').addClass('d-none');
        $('.icon-remove-wo').removeClass('d-none');
    });
});

// Changes color on hover
$(function () {
    $('button.btn-edit').hover(function () {
    $('.icon-edit').addClass('d-none');
    $('.icon-edit-focus').removeClass('d-none');
    },
    function () {
        $('.icon-edit-focus').addClass('d-none');
        $('.icon-edit').removeClass('d-none');
    });
});

// dropdown
const targetDiv = document.getElementById("myDropdown");
const btn = document.getElementById("dropdown-btn");

function myFunction() {
    if (targetDiv.style.display !== "none") {
        targetDiv.style.display = "none";
        document.getElementById("dropdown-btn").classList.toggle("dropbtn-active");
    } else {
    targetDiv.style.display = "block";
    btn.classList.remove('dropbtn-active');
    }
};