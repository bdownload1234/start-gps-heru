$(document).ready(() => {

    $('.category').on('change', function() {
      if (this.value == 1) {
        $('.form-start').addClass('d-none');
        $('.form-fleet').removeClass('d-none');
      }
    });

    $(".dropdown-search, .dropdown-search2, #event-from, #event-to").on("change", function() {
      // alert($('.dropdown-search').val());
      reportval();
    });

    function reportval() {
      var can = false;
      $(".dropdown-search, .dropdown-search2, #event-from, #event-to").each(function() {
        console.log($(this).val());
        if ($('.dropdown-search').val() != '' && $('.dropdown-search2').val() != '' && $('#event-to').val() != '' && $('#event-from').val() != '') {
          can = true;
        }
      });
      if (can) {
        $('.report-submit').css({
          background: '#0073E5'
        });
        // $('.report-submit').bind('click.poistore', function() {poistore();});
        // $('.report-submit').bind('click.poiredirect', function() {window.location.href='index-report-sub.html';});
      } else {
        $('.report-submit').css({
          background: '#C2C2C2'
        });
        // $('.report-submit').unbind('click.poistore');
        // $('.report-submit').unbind('click.poiredirect');
      }
    };

    $(".dropdown-search-fleet, .dropdown-search2-fleet").on("change", function() {
      // alert($('.dropdown-search').val());
      fleetVal();
    });

    function fleetVal() {
      var can = false;
      $(".dropdown-search-fleet, .dropdown-search2-fleet").each(function() {
        console.log($(this).val());
        if ($('.dropdown-search-fleet').val() != '' && $('.dropdown-search2-fleet').val() != '') {
          can = true;
        }
      });
      if (can) {
        $('.report-submit-fleet').css({
          background: '#0073E5'
        });
        // $('.report-submit-fleet').bind('click.poistore', function() {poistore();});
        // $('.report-submit-fleet').bind('click.fleetredirect', function() {window.location.href='index-report-fleet-monitoring.html';});
      } else {
        $('.report-submit-fleet').css({
          background: '#C2C2C2'
        });
        // $('.report-submit-fleet').unbind('click.poistore');
        // $('.report-submit-fleet').unbind('click.fleetredirect');
      }
    };

    $('.dropdown-search').selectize({
    searchField: 'title',
    valueField: 'value',
    placeholder: 'Pilih kategori report',
    options: [
        {value: 1, title: 'Fleet Report Summary'},
        {value: 2, title: 'Route & Trip Monitoring'},
        {value: 3, title: 'Fleet Monitoring'},
        {value: 4, title: 'Notification Monitoring'}
    ],
    render: {
        option: function(data, escape) {
            return '<div class="option"' +
                    '<span class="title font-14">' + escape(data.title) + '</span>' +
                    '</div>';
        },
        item: function(data, escape) {
            return '<div class="item"><i class="fas fa-fw ' + ' text-secondary font-12 me-1"></i>' + escape(data.title) + '</div>';
        }
    }
  });

  $('.dropdown-search2').selectize({
    searchField: 'title',
    valueField: 'value',
    placeholder: 'Pilih kendaraan',
    options: [
        {value: 1, title: 'Fleet Report Summary'},
        {value: 2, title: 'Route & Trip Monitoring'},
        {value: 3, title: 'Fleet Monitoring'},
        {value: 4, title: 'Notification Monitoring'}
    ],
    render: {
        option: function(data, escape) {
            return '<div class="option"' +
                    '<span class="title font-14">' + escape(data.title) + '</span>' +
                    '</div>';
        },
        item: function(data, escape) {
            return '<div class="item"><i class="fas fa-fw ' + ' text-secondary font-12 me-1"></i>' + escape(data.title) + '</div>';
        }
    }
  });

  $('.dropdown-search-fleet').selectize({
    searchField: 'title',
    valueField: 'value',
    placeholder: 'Pilih kategori report',
    items : [1],
    options: [
        {value: 1, title: 'Fleet Report Summary'},
        {value: 2, title: 'Route & Trip Monitoring'},
        {value: 3, title: 'Fleet Monitoring'},
        {value: 4, title: 'Notification Monitoring'}
    ],
    render: {
        option: function(data, escape) {
            return '<div class="option"' +
                    '<span class="title font-14">' + escape(data.title) + '</span>' +
                    '</div>';
        },
        item: function(data, escape) {
            return '<div class="item"><i class="fas fa-fw ' + ' text-secondary font-12 me-1"></i>' + escape(data.title) + '</div>';
        }
    }
  });

  $('.dropdown-search2-fleet').selectize({
    searchField: 'title',
    valueField: 'value',
    placeholder: 'Pilih kendaraan',
    items : [1],
    options: [
        {value: 1, title: 'Mobil 1 - Fleet - Activity'},
        {value: 2, title: 'Mobil 2 - Fleet - Activity'},
        {value: 3, title: 'Mobil 3 - Fleet - Activity'},
        {value: 4, title: 'Mobil 4 - Fleet - Activity'}
    ],
    render: {
        option: function(data, escape) {
            return '<div class="option"' +
                    '<span class="title font-14">' + escape(data.title) + '</span>' +
                    '</div>';
        },
        item: function(data, escape) {
            return '<div class="item"><i class="fas fa-fw ' + ' text-secondary font-12 me-1"></i>' + escape(data.title) + '</div>';
        }
    }
  });

  $('.report-submit').click(function() {
    if ($('.dropdown-search').val() == '') {
      $('.dropdown-search > .items').addClass("selectize-input-invalid");
      $('.dropdown-search > .selectize-input').addClass("hidden");
      $('.invalid-selectize-1').removeClass("d-none");
    } else{
      // if ($('.dropdown-search').val() == "1"){
      //   document.getElementsByClassName("report-submit").onclick = function () {
      //       window.location.href = 'index-report-fleet-monitoring.html';
      //   };
      // }
      $('.dropdown-search > .items').removeClass("dropdown-search > selectize-input-invalid");
      $('.dropdown-search > .selectize-input').removeClass("hidden");
      $('.invalid-selectize-1').addClass("d-none");
    }

    if ($('.dropdown-search2').val() == '') {
      $('.dropdown-search2 > .items').addClass("selectize-input-invalid");
      $('.dropdown-search2 > .selectize-input').addClass("hidden");
      $('.invalid-selectize-2').removeClass("d-none");
    } else{
      $('.dropdown-search2 > .items').removeClass("dropdown-search2 > selectize-input-invalid");
      $('.dropdown-search2 > .selectize-input').removeClass("hidden");
      $('.invalid-selectize-2').addClass("d-none");
    }
  });

  

  $('.report-submit-fleet').click(function() {
    if ($('.dropdown-search-fleet').val() == '') {
      $('.dropdown-search-fleet > .items').addClass("selectize-input-invalid");
      $('.dropdown-search-fleet > .selectize-input').addClass("hidden");
      $('.invalid-selectize-1').removeClass("d-none");
    } else{
        $('.dropdown-search-fleet > .items').removeClass("dropdown-search-fleet > selectize-input-invalid");
        $('.dropdown-search-fleet > .selectize-input').removeClass("hidden");
        $('.invalid-selectize-1').addClass("d-none");
    }

    if ($('.dropdown-search2-fleet').val() == '') {
      $('.dropdown-search2-fleet > .items').addClass("selectize-input-invalid");
      $('.dropdown-search2-fleet > .selectize-input').addClass("hidden");
      $('.invalid-selectize-2').removeClass("d-none");
    } else{
      $('.dropdown-search2-fleet > .items').removeClass("dropdown-search2-fleet > selectize-input-invalid");
      $('.dropdown-search2-fleet > .selectize-input').removeClass("hidden");
      $('.invalid-selectize-2').addClass("d-none");
    }

    // if ($('.dropdown-search-fleet').val() == 1) {
    //     document.getElementsByClassName("report-submit-fleet").onclick = function () {
    //         window.location.href = 'index-report-fleet-monitoring.html';
    //     };
    // }else{
    //   document.getElementsByClassName("report-submit-fleet").onclick = function () {
    //     window.location.href = 'index-report-fleet-monitoring.html';
    //   };
    // }

    // if ($('.dropdown-search-fleet').val() == 1){
    //   $('.report-submit-fleet').bind('click.fleetredirect', function() {window.location.href='index-report-fleet-monitoring.html';});
    // }
  });

  // $('.report-submit-fleet').bind('click.fleetredirect', function() {
  //   if ($('.dropdown-search-fleet').val() == 1 && $('.dropdown-search2-fleet').val() != '')  {
  //     window.location.href = 'index-report-fleet-monitoring.html';
  //   }
  // });

  // if ($('.dropdown-search-fleet').val() == 1){
  //   // $('.report-submit-fleet').bind('click.fleetredirect', function() {window.location.href='index-report-fleet-monitoring.html';});
  //   //   document.getElementsByClassName("report-submit").onclick = function () {
  //     //       window.location.href = 'index-report-fleet-monitoring.html';
  //     //   };
  // }else{
  //   $('.report-submit-fleet').unbind('click.fleetredirect');
  // }

  $(".dropdown-search" ).change(function() {
    // alert($(this).value);
    if ($(this).val() != '') {
        $('.dropdown-search > .items').removeClass("dropdown-search > selectize-input-invalid");
        $('.dropdown-search > .selectize-input').removeClass("hidden");
        $('.invalid-selectize-1').addClass("d-none");
      }else{
        $('.dropdown-search > .items').addClass("dropdown-search > selectize-input-invalid");
        $('.dropdown-search > .selectize-input').addClass("hidden");
        $('.invalid-selectize-1').removeClass("d-none");
      }
  });

  $(".dropdown-search-fleet" ).change(function() {
    // alert($(this).value);
    if ($(this).val() != '') {
        $('.dropdown-search-fleet > .items').removeClass("dropdown-search-fleet > selectize-input-invalid");
        $('.dropdown-search-fleet > .items').addClass("dropdown-search-fleet > selectize-input:after");
        $('.invalid-selectize-1').addClass("d-none");
      }
  });

  $(".dropdown-search2" ).change(function() {
    if ($(this).val() != '') {
        $('.dropdown-search2 > .items').removeClass("dropdown-search > selectize-input-invalid");
        $('.dropdown-search2 > .selectize-input').removeClass("hidden");
        $('.invalid-selectize-2').addClass("d-none");
      }else{
        $('.dropdown-search2 > .items').addClass("dropdown-search2 > selectize-input-invalid");
        $('.dropdown-search2 > .selectize-input').addClass("hidden");
        $('.invalid-selectize-2').removeClass("d-none");
      }
  });

  $(".dropdown-search2-fleet" ).change(function() {
    if ($(this).val() != '') {
        $('.dropdown-search2-fleet > .items').removeClass("dropdown-search-fleet > selectize-input-invalid");
        $('.dropdown-search2-fleet > .items').addClass("dropdown-search2-fleet > selectize-input:after");
        $('.invalid-selectize-2').addClass("d-none");
      }
  });

  $(function () {
    $('.btn-create').hover(function () {
      $('.icon-bottom-create').addClass('d-none');
      $('.icon-bottom-create-focus').removeClass('d-none');
      $('.btn-create').removeClass('text-dark');
      $('.btn-create').addClass('text-blue-hover');
    },
      function () {
        $('.icon-bottom-create-focus').addClass('d-none');
        $('.icon-bottom-create').removeClass('d-none');
        $('.btn-create').addClass('text-dark');
      });
  });

  $('#event-from').datepicker({
    dayNamesMin: ['S', 'M', 'T', 'W', 'T', 'F', 'S'],
    dateFormat: "dd MM yy"
  });
  $('#event-to').datepicker({
    dayNamesMin: ['S', 'M', 'T', 'W', 'T', 'F', 'S'],
    dateFormat: "dd MM yy"
  });

  (function () {
    'use strict'

    // Fetch all the forms we want to apply custom Bootstrap validation styles to
    var forms = document.querySelectorAll('.needs-validation')
    var dropdown_search = $("#dropdown-search");
    var dropdown_search2 = $("#dropdown-search2");

    // Loop over them and prevent submission
    Array.prototype.slice.call(forms)
      .forEach(function (form) {
        form.addEventListener('submit', function (event) {
          if (!form.checkValidity() || (dropdown_search.val() == '') || (dropdown_search2.val() == '') ) {
            event.preventDefault()
            event.stopPropagation()
          }

          form.classList.add('was-validated')
        }, false)
      })
  })();

  (function () {
    'use strict'

    // Fetch all the forms we want to apply custom Bootstrap validation styles to
    var forms = document.querySelectorAll('.needs-validation-fleet')
    var dropdown_search = $("#dropdown-search-fleet");
    var dropdown_search2 = $("#dropdown-search2-fleet");

    // Loop over them and prevent submission
    Array.prototype.slice.call(forms)
      .forEach(function (form) {
        form.addEventListener('submit', function (event) {
          if (!form.checkValidity() || (dropdown_search.val() == '') || (dropdown_search2.val() == '') ) {
            event.preventDefault()
            event.stopPropagation()
          }

          form.classList.add('was-validated')
        }, false)
      })
  })();
    
}); 