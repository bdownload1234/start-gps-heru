// Avoid dropdown menu from closing when clicking inside dropdown
$('.dropdown-menu').on('click', function(event) {
  event.stopPropagation();
});

$('#vehicle-search-filter-dropdown input[type=checkbox]').on('change', function() {
  if($(this).is(':checked') === true) {
    $('#vehicle-search-filter-dropdown button').removeClass('btn-secondary disabled');
    $('#vehicle-search-filter-dropdown button').addClass('btn-primary');
  }
  else{
    $('#vehicle-search-filter-dropdown button').addClass('btn-secondary disabled');
  }

  var checkedBox = $('input:checkbox:checked').length;
  $('#filtercount').text(checkedBox);
  if (checkedBox > 0) {
    $('#filtercount').show()
  } else {
    $('#filtercount').hide()
  }
});

// bottom fleet collapse
$('.line-collapse').on('click', function() {
  $('.dataTables_scrollBody, .monitoring-bottom-info').toggleClass('active');
});

// Sidebar Collapse
$('.monitoring-sidebar-button-collapse, .monitoring-sidebar-sticky-badge').on('click', function() {
  $('.monitoring-sidebar').toggleClass('minimize');
  $('.monitoring-sidebar-button-collapse, .monitoring-sidebar-sticky-badge').toggleClass('active');
});


// Bottom Info Collapse
$('.bottom-info-button-collapse, .monitoring-bottom-info-sticky-badge').on('click', function() {
  $('.bottom-info').toggleClass('minimize');
  $('.bottom-info-button-collapse, .monitoring-bottom-info-sticky-badge').toggleClass('active');
});

// Car item when click remove class opacity at bottom info
$('.car-item a').on('click', function() {
  $('.bottom-info').removeClass('opacity-0');
  $('.bottom-info-button-collapse').removeClass('opacity-0');
});

// Tooltip Maps Only
const tooltipTriggerMaps = [].slice.call(document.querySelectorAll('[data-bs-toggle-maps="tooltip"]'))
tooltipTriggerMaps.map(function (tooltipTriggerEl) {
  return new bootstrap.Tooltip(tooltipTriggerEl, {
    'customClass': 'maps-tooltip',
    'trigger': 'hover'
  });
});

// Tooltip global, except maps
const tooltipTriggerGlobal = [].slice.call(document.querySelectorAll('[data-bs-toggle="tooltip"]'))
tooltipTriggerGlobal.map(function (tooltipTriggerEl) {
  return new bootstrap.Tooltip(tooltipTriggerEl);
});