$(function () {
  $('[data-toggle="tooltip"]').tooltip();
});

var countChecked = function ($table, checkboxClass) {
  if ($table) {
    // Find all elements with given class
    var chkAll = $table.find(checkboxClass);
    // Count checked checkboxes
    var checked = chkAll.filter(":checked").length;
    // Count total
    var total = chkAll.length;
    // Return an object with total and checked values
    return {
      total: total,
      checked: checked,
    };
  }
};



// Setup
var notification = new Notif({
  topPos: 10,
  classNames: "success danger",
  autoClose: false,
  autoCloseTimeout: 3000,
});

function Notif(option) {
  var el = this;

  el.self = $(".toast-message");
  el.close = this.self.find(".close");
  el.message = el.self.find(".message");
  el.top = option.topPos;
  el.classNames = option.classNames;
  el.autoClose =
    typeof option.autoClose === "boolean" ? option.autoClose : false;
  el.autoCloseTimeout =
    option.autoClose && typeof option.autoCloseTimeout === "number"
      ? option.autoCloseTimeout
      : 3000;

  // Methods
  el.reset = function () {
    el.message.empty();
    el.self.removeClass(el.classNames);
  };
  el.show = function (msg, type) {
    el.reset();
    el.self.css("display", "block");
    el.message.text(msg);
    el.self.addClass(type);

    if (el.autoClose) {
      setTimeout(function () {
        el.hide();
      }, el.autoCloseTimeout);
    }
  };
  el.hide = function () {
    el.self.css("display", "none");
    el.reset();
  };

  el.close.on("click", this.hide);
}

var justSubmitted = sessionStorage.getItem("notif");
if (justSubmitted != "null") {
    notification.show('Satu fleet telah ditambahkan', 'success');
    $('table.table-created > tbody').append(`
    <tr class="for-delete-purpose">
                        <td></td>
                        <td>
                          <div class="tabtitle">
                            <div class="wd-30">
                              <img src="assets/img/misc/hino-brand.png" alt="hino-brand">
                            </div>
                            <div class="wd-60">
                              <a href="md-fleet-detail.html" class="link-style">
                                <div class="tabletitle">FM 260 JD</div>
                              </a>
                              Hino
                            </div>
                          </div>
                        </td>
                        <td>
                          <div class="tabletitle">
                            <a data-toggle="tooltip" data-placement="top" title="Type 1"><img src="assets/img/misc/sumbu-fleet.png" alt="sumbu-1" class="img-sumbu"></a>
                            <img src="assets/img/misc/sumbu-fleet.png" alt="sumbu-1" class="img-sumbu">
                          </div>
                          2 Sumbu
                        </td>
                        <td>
                          <div class="tabletitle">
                            1300 cc
                          </div>
                          25 liter
                        </td>
                        <td>
                          <div class="tabletitle">
                            Amir
                          </div>
                          23 Desemmber 2021
                        </td>
                        <td></td>
                      </tr>
    `);
    console.log(justSubmitted);
  // delete the cookie
  sessionStorage.setItem("notif", null);
}


// table created
var tableCreated = $("table.table-created").DataTable({
  scrollCollapse: true,
  autoWidth: false,
  columnDefs: [
    {
      width: "24px",
      className: "dt-body-center",
      targets: 0,
      searchable: false,
      orderable: false,
      render: function (data, type, full, meta) {
        return (
          '<input type="checkbox" class="created-check-element" value="' +
          $("<div/>").text(data).html() +
          '">'
        );
      },
    },
    {
      orderable: false,
      width: "5px",
      targets: -1,
      render: function (data, type, row, meta) {
        return `<button type="button" class="btn btn-remove btn-link p-0" id="n-${meta.row}" data-bs-toggle="modal" data-bs-target="#delete-modal"/>
            <img src="assets/img/misc/remove-wo.png" alt="" class="icon-remove-wo">
            <img src="assets/img/misc/remove-wo-focus.png" alt="" class="icon-remove-wo-focus d-none">
        </button>`;
      },
    },
  ],
  order: [1, "asc"],
  paging: true,
  lengthChange: true,
  showNEntries: true,
  searching: true,
  bInfo: true,
  language: {
    lengthMenu: ``,
    info: `_TOTAL_ Data <div class="line-data"></div>`,
    sInfoFiltered: "",
    search: "_INPUT_",
    searchPlaceholder: "Cari data",
    emptyTable: `
    <img src="assets/img/misc/no-data-table.svg" class="d-block mx-auto mb-3">
    <label>Belum ada data yang tersedia, <a class="text-link">Create Data</a></label>
    `,
  },
  infoCallback: function (settings, start, end, max, total, pre) {
    if (total == 0) return "" + total + ' Data <div class="line-data"></div>';
    return "" + total + ' Data <div class="line-data"></div>';
  },
});

$('.delete-fleet').click(function(){
  tableCreated
        .row( $('.created-check-element:checked').parents('.for-delete-purpose') )
        .remove()
        .draw();

  $('.created-check-element').prop('checked',false);
  var result = countChecked($(".table-created"), ".created-check-element");
  if (result.checked != 0) {
    $(
      "#table-wrapper-created > #measurement-table_wrapper > :nth-child(3) .dataTables_info"
    ).addClass("d-none");
    $(
      "#table-wrapper-created > #measurement-table_wrapper > div:nth-child(1) .dataTables_length"
    ).addClass("d-none");
    $("#checked").removeClass("d-none");
    $(
      "#table-wrapper-created > #measurement-table_wrapper > div:nth-child(2) table"
    ).addClass("mt-54");
    $("#checked").css({
      position: "absolute",
      top: "250px",
    });
    $("#checked").html(
      result.checked +
        ' Terseleksi <div class="line-data"></div>' +
        `<button type="button" class="btn btn-rmv-checked" data-bs-toggle="modal" data-bs-target="#delete-modal-table" style= "margin-top: -6px; padding: 6px;">
                            <img src="assets/img/misc/trash2.png" alt="" style="margin-bottom: 2px;" class="icon-remove-checked" width= "14.4px" height="14.4px">
                            <img src="assets/img/misc/trash2-focus.png" alt="" style="margin-bottom: 2px;" class="icon-remove-checked-focus d-none" width= "14.4px" height="14.4px">
                            Cancel
                            </button>`
    );
  } else {
    $(
      "#table-wrapper-created > #measurement-table_wrapper > div:nth-child(2) table"
    ).removeClass("mt-54");
    $("#checked").addClass("d-none");
    $(
      "#table-wrapper-created > #measurement-table_wrapper > :nth-child(3) .dataTables_info"
    ).removeClass("d-none");
    $(
      "#table-wrapper-created > #measurement-table_wrapper > div:nth-child(1) .dataTables_length"
    ).removeClass("d-none");
  }
});


$(document).on("change", ".created-check-element", function () {
  var result = countChecked($(".table-created"), ".created-check-element");
  if (result.checked != 0) {
    $(
      "#table-wrapper-created > #measurement-table_wrapper > :nth-child(3) .dataTables_info"
    ).addClass("d-none");
    $(
      "#table-wrapper-created > #measurement-table_wrapper > div:nth-child(1) .dataTables_length"
    ).addClass("d-none");
    $("#checked").removeClass("d-none");
    $(
      "#table-wrapper-created > #measurement-table_wrapper > div:nth-child(2) table"
    ).addClass("mt-54");
    $("#checked").css({
      position: "absolute",
      top: "250px",
    });
    $("#checked").html(
      result.checked +
        ' Terseleksi <div class="line-data"></div>' +
        `<button type="button" class="btn btn-rmv-checked" data-bs-toggle="modal" data-bs-target="#delete-modal-table" style= "margin-top: -6px; padding: 6px;">
                            <img src="assets/img/misc/trash2.png" alt="" style="margin-bottom: 2px;" class="icon-remove-checked" width= "14.4px" height="14.4px">
                            <img src="assets/img/misc/trash2-focus.png" alt="" style="margin-bottom: 2px;" class="icon-remove-checked-focus d-none" width= "14.4px" height="14.4px">
                            Cancel
                            </button>`
    );
  } else {
    $(
      "#table-wrapper-created > #measurement-table_wrapper > div:nth-child(2) table"
    ).removeClass("mt-54");
    $("#checked").addClass("d-none");
    $(
      "#table-wrapper-created > #measurement-table_wrapper > :nth-child(3) .dataTables_info"
    ).removeClass("d-none");
    $(
      "#table-wrapper-created > #measurement-table_wrapper > div:nth-child(1) .dataTables_length"
    ).removeClass("d-none");
  }
  // $('#total').html(result.total);
});

$(document).on("change", "#created-select-all", function () {
  var result = countChecked($(".table-created"), ".created-check-element");
  if (result.checked != 0) {
    $(
      "#table-wrapper-created > #measurement-table_wrapper > :nth-child(3) .dataTables_info"
    ).addClass("d-none");
    $(
      "#table-wrapper-created > #measurement-table_wrapper > div:nth-child(1) .dataTables_length"
    ).addClass("d-none");
    $("#checked").removeClass("d-none");
    $(
      "#table-wrapper-created > #measurement-table_wrapper > div:nth-child(2) table"
    ).addClass("mt-54");
    $("#checked").css({
      position: "absolute",
      top: "250px",
    });
    $("#checked").html(
      result.checked +
        ' Terseleksi <div class="line-data"></div>' +
        `<button type="button" class="btn btn-rmv-checked" data-bs-toggle="modal" data-bs-target="#delete-modal-table" style= "margin-top: -6px; padding: 6px;">
                            <img src="assets/img/misc/trash2.png" alt="" style="margin-bottom: 2px;" class="icon-remove-checked" width= "14.4px" height="14.4px">
                            <img src="assets/img/misc/trash2-focus.png" alt="" style="margin-bottom: 2px;" class="icon-remove-checked-focus d-none" width= "14.4px" height="14.4px">
                            Cancel
                            </button>`
    );
  } else {
    $(
      "#table-wrapper-created > #measurement-table_wrapper > div:nth-child(2) table"
    ).removeClass("mt-54");
    $("#checked").addClass("d-none");
    $(
      "#table-wrapper-created > #measurement-table_wrapper > :nth-child(3) .dataTables_info"
    ).removeClass("d-none");
    $(
      "#table-wrapper-created > #measurement-table_wrapper > div:nth-child(1) .dataTables_length"
    ).removeClass("d-none");
  }
  // $('#total').html(result.total);
});

// Handle click on "Select all" control
$("#created-select-all").on("click", function () {
  // Check/uncheck all checkboxes in the table
  var rows = tableCreated.rows({ search: "applied" }).nodes();
  $('input[type="checkbox"]', rows).prop("checked", this.checked);
});

// Handle click on checkbox to set state of "Select all" control
$("table.table-created tbody").on(
  "change",
  'input[type="checkbox"]',
  function () {
    // If checkbox is not checked
    if (!this.checked) {
      var el = $("#created-select-all").get(0);
      // If "Select all" control is checked and has 'indeterminate' property
      if (el && el.checked && "indeterminate" in el) {
        // Set visual state of "Select all" control
        // as 'indeterminate'
        el.indeterminate = true;
      }
    }
  }
);

// table progress
var tableProgress = $("table.table-progress").DataTable({
  scrollCollapse: true,
  autoWidth: false,
  columnDefs: [
    {
      width: "24px",
      className: "dt-body-center",
      targets: 0,
      searchable: false,
      orderable: false,
      render: function (data, type, full, meta) {
        return (
          '<input type="checkbox" class="progress-check-element" value="' +
          $("<div/>").text(data).html() +
          '">'
        );
      },
    },
    {
      orderable: false,
      width: "5px",
      targets: -1,
      render: function (data, type, row, meta) {
        return `<button type="button" class="btn btn-remove btn-link p-0" id="n-${meta.row}" data-bs-toggle="modal" data-bs-target="#delete-modal"/>
                <img src="assets/img/misc/remove-wo.png" alt="" class="icon-remove-wo">
                <img src="assets/img/misc/remove-wo-focus.png" alt="" class="icon-remove-wo-focus d-none">
            </button>`;
      },
    },
  ],
  order: [1, "asc"],
  paging: true,
  lengthChange: true,
  showNEntries: true,
  searching: true,
  bInfo: true,
  // "bPaginate": false,
  language: {
    lengthMenu: ``,
    info: `_TOTAL_ Data <div class="line-data"></div>`,
    sInfoFiltered: "",
    search: "_INPUT_",
    searchPlaceholder: "Cari data",
    emptyTable: `
        <img src="assets/img/misc/no-data-table.svg" class="d-block mx-auto mb-3">
        <label>Belum ada data yang tersedia, <a class="text-link">Create Data</a></label>
        `,
  },
  infoCallback: function (settings, start, end, max, total, pre) {
    if (total == 0) return "" + total + ' Data <div class="line-data"></div>';
    return "" + total + ' Data <div class="line-data"></div>';
  },
});

$(document).on("change", ".progress-check-element", function () {
  var result = countChecked($(".table-progress"), ".progress-check-element");
  if (result.checked != 0) {
    $(
      "#table-wrapper-progress > #measurement-table_wrapper > :nth-child(3) .dataTables_info"
    ).addClass("d-none");
    $(
      "#table-wrapper-progress > #measurement-table_wrapper > div:nth-child(1) .dataTables_length"
    ).addClass("d-none");
    $("#checked-progress").removeClass("d-none");
    $(
      "#table-wrapper-progress > #measurement-table_wrapper > div:nth-child(2) table"
    ).addClass("mt-54");
    $("#checked-progress").css({
      position: "absolute",
      top: "250px",
    });
    $("#checked-progress").html(
      result.checked +
        ' Terseleksi <div class="line-data"></div>' +
        `<button type="button" class="btn btn-rmv-checked" data-bs-toggle="modal" data-bs-target="#delete-modal-table" style= "margin-top: -6px; padding: 6px;">
                            <img src="assets/img/misc/trash2.png" alt="" style="margin-bottom: 2px;" class="icon-remove-checked" width= "14.4px" height="14.4px">
                            <img src="assets/img/misc/trash2-focus.png" alt="" style="margin-bottom: 2px;" class="icon-remove-checked-focus d-none" width= "14.4px" height="14.4px">
                             Cancel
                            </button>`
    );
  } else {
    $(
      "#table-wrapper-progress > #measurement-table_wrapper > div:nth-child(2) table"
    ).removeClass("mt-54");
    $("#checked-progress").addClass("d-none");
    $(
      "#table-wrapper-progress > #measurement-table_wrapper > :nth-child(3) .dataTables_info"
    ).removeClass("d-none");
    $(
      "#table-wrapper-progress > #measurement-table_wrapper > div:nth-child(1) .dataTables_length"
    ).removeClass("d-none");
  }
  // $('#total').html(result.total);
});

$(document).on("change", "#progress-select-all", function () {
  var result = countChecked($(".table-progress"), ".progress-check-element");
  if (result.checked != 0) {
    $(
      "#table-wrapper-progress > #measurement-table_wrapper > :nth-child(3) .dataTables_info"
    ).addClass("d-none");
    $(
      "#table-wrapper-progress > #measurement-table_wrapper > div:nth-child(1) .dataTables_length"
    ).addClass("d-none");
    $("#checked-progress").removeClass("d-none");
    $(
      "#table-wrapper-progress > #measurement-table_wrapper > div:nth-child(2) table"
    ).addClass("mt-54");
    $("#checked-progress").css({
      position: "absolute",
      top: "250px",
    });
    $("#checked-progress").html(
      result.checked +
        ' Terseleksi <div class="line-data"></div>' +
        `<button type="button" class="btn btn-rmv-checked" data-bs-toggle="modal" data-bs-target="#delete-modal-table" style= "margin-top: -6px; padding: 6px;">
                            <img src="assets/img/misc/trash2.png" alt="" style="margin-bottom: 2px;" class="icon-remove-checked" width= "14.4px" height="14.4px">
                            <img src="assets/img/misc/trash2-focus.png" alt="" style="margin-bottom: 2px;" class="icon-remove-checked-focus d-none" width= "14.4px" height="14.4px">
                             Cancel
                            </button>`
    );
  } else {
    $(
      "#table-wrapper-progress > #measurement-table_wrapper > div:nth-child(2) table"
    ).removeClass("mt-54");
    $("#checked-progress").addClass("d-none");
    $(
      "#table-wrapper-progress > #measurement-table_wrapper > :nth-child(3) .dataTables_info"
    ).removeClass("d-none");
    $(
      "#table-wrapper-progress > #measurement-table_wrapper > div:nth-child(1) .dataTables_length"
    ).removeClass("d-none");
  }
  // $('#total').html(result.total);
});

// Handle click on "Select all" control
$("#progress-select-all").on("click", function () {
  // Check/uncheck all checkboxes in the table
  var rowsP = tableProgress.rows({ search: "applied" }).nodes();
  $('input[type="checkbox"]', rowsP).prop("checked", this.checked);
});

// Handle click on checkbox to set state of "Select all" control
$("table.table-progress tbody").on(
  "change",
  'input[type="checkbox"]',
  function () {
    // If checkbox is not checked
    if (!this.checked) {
      var el = $("#progress-select-all").get(0);
      // If "Select all" control is checked and has 'indeterminate' property
      if (el && el.checked && "indeterminate" in el) {
        // Set visual state of "Select all" control
        // as 'indeterminate'
        el.indeterminate = true;
      }
    }
  }
);

$("#measurement-table_filter label").addClass("p-relative");
$("#measurement-table_filter label input").css({ "padding-left": "2.2rem" });
$("#measurement-table_filter label").append(`
    <img class="fas fa-fw fa-search p-absolute text-secondary" style="left: 21px; top: 11px; display: flex; align-items: center; width: 15px; height: 15px;" src= "assets/img/misc/search-icon2.png"></img>
    `);

$("#created #measurement-table_length").append(`
    <div class="filter-title">Filter</div>
    <select id="dropdown-search" class="form-control shadow-none filter-table selectpicker font-14" required></select>
`);

$("#disable-table #measurement-table_length").append(`
    <div class="filter-title">Filter</div>
    <select id="dropdown-search" class="form-control shadow-none filter-table selectpicker font-14" required></select>
`);

$(".filter-table").selectize({
  searchField: "title",
  valueField: "value",
  placeholder: "Pilih satu",
  items: [1],
  options: [
    { value: 1, title: "Option 1" },
    { value: 2, title: "Option 2" },
    { value: 3, title: "Option 3" },
    { value: 4, title: "Option 4" },
  ],
  render: {
    option: function (data, escape) {
      return (
        '<div class="option"' +
        '<span class="title font-14">' +
        escape(data.title) +
        "</span>" +
        "</div>"
      );
    },
    item: function (data, escape) {
      return '<div class="item">' + escape(data.title) + "</div>";
    },
  },
});

// Changes color on hover
$(function () {
  $("a.btn-create").hover(
    function () {
      $(".icon-bottom-create").addClass("d-none");
      $(".icon-bottom-create-focus").removeClass("d-none");
    },
    function () {
      $(".icon-bottom-create-focus").addClass("d-none");
      $(".icon-bottom-create").removeClass("d-none");
    }
  );
});

// Changes color on hover
$(function () {
  $("button.btn-remove").hover(
    function () {
      $(".icon-remove-wo").addClass("d-none");
      $(".icon-remove-wo-focus").removeClass("d-none");
    },
    function () {
      $(".icon-remove-wo-focus").addClass("d-none");
      $(".icon-remove-wo").removeClass("d-none");
    }
  );
});

// Changes color on hover
$(function () {
  $("button.btn-edit").hover(
    function () {
      $(".icon-edit").addClass("d-none");
      $(".icon-edit-focus").removeClass("d-none");
    },
    function () {
      $(".icon-edit-focus").addClass("d-none");
      $(".icon-edit").removeClass("d-none");
    }
  );
});

// dropdown
const targetDiv = document.getElementById("myDropdown");
const btn = document.getElementById("dropdown-btn");

function myFunction() {
  if (targetDiv.style.display !== "none") {
    targetDiv.style.display = "none";
    document.getElementById("dropdown-btn").classList.toggle("dropbtn-active");
  } else {
    targetDiv.style.display = "block";
    btn.classList.remove("dropbtn-active");
  }
}
