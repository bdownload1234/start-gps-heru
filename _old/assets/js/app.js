// Avoid dropdown menu from closing when clicking inside dropdown
$('.dropdown-menu').on('click', function(event) {
  event.stopPropagation();
});

// Sidebar Collapse
$('.main-section button.sidebar-collapse-btn').on('click', function() {
  $('.main-section .sidebar').toggleClass('collapsed');
});

// Bottom Info Collapse
$('.main-section button.bottom-info-collapse-btn').on('click', function() {
  $('.main-section .bottom-info').toggleClass('collapsed');
});