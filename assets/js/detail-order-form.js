$(document).ready(() => {

      Selectize.define('option_click', function(options) {
        var self = this;
        var setup = self.setup;
        this.setup = function() {
            setup.apply(self, arguments);

            var clicking = false;

            // Detect click on a .clickable
            self.$dropdown_content.on('mousedown click', function(e) {
                if ($(e.target).hasClass('clickable')) {
                    if (e.type === 'mousedown') {
                        clicking = true;
                        self.isFocused = false; // awful hack to defuse the document mousedown listener
                    } else {
                        self.isFocused = true;
                        setTimeout(function() {
                            clicking = false; // wait until blur has been preempted
                        });
                    }
                } else { // cleanup in case user right-clicked or dragged off the element
                    clicking = false;
                    self.isFocused = true;
                }
            });

            // Intercept default handlers
            self.$dropdown.off('mousedown click', '[data-selectable]').on('mousedown click', '[data-selectable]', function() {
                if (!clicking) {
                    return self.onOptionSelect.apply(self, arguments);
                }
            });
            self.$control_input.off('blur').on('blur', function() {
                if (!clicking) {
                    return self.onBlur.apply(self, arguments);
                }
            });
        }
    });

    $('.fleet-to-dropdown').selectize({
      searchField: 'title',
      valueField: 'value',
      placeholder: 'Pilih satu',
      // items : [],
      options: [
          {value: 1, title: 'Fleet 1'},
          {value: 2, title: 'Fleet 2'},
          {value: 3, title: 'Fleet 3'},
          {value: 4, title: 'Fleet 4'},
          {value: 5, title: 'Terisi Otomatis'}
      ],
      render: {
          option: function(data, escape) {
              return '<div class="option"' +
                      '<span class="title font-14">' + escape(data.title) + '</span>' +
                      '</div>';
          },
          item: function(data, escape) {
              return '<div class="item">' + escape(data.title) + '</div>';
          }
      },
      
    });

    $('.fleet-id-to-dropdown').selectize({
      searchField: 'title',
      valueField: 'value',
      placeholder: 'Pilih satu',
      // items : [1],
      options: [
          {value: 1, title: 'Fleet 1'},
          {value: 2, title: 'Fleet 2'},
          {value: 3, title: 'Fleet 3'},
          {value: 4, title: 'Fleet 4'},
          {value: 5, title: 'Terisi Otomatis'}
      ],
      render: {
          option: function(data, escape) {
              return '<div class="option"' +
                      '<span class="title font-14">' + escape(data.title) + '</span>' +
                      '</div>';
          },
          item: function(data, escape) {
              return '<div class="item">' + escape(data.title) + '</div>';
          }
      }
    });

    $('.fleet-from-dropdown').selectize({
      searchField: 'title',
      valueField: 'value',
      placeholder: 'Pilih satu',
      // items : [1],
      options: [
          {value: 1, title: 'Fleet 1'},
          {value: 2, title: 'Fleet 2'},
          {value: 3, title: 'Fleet 3'},
          {value: 4, title: 'Fleet 4'}
      ],
      render: {
          option: function(data, escape) {
              return '<div class="option"' +
                      '<span class="title font-14">' + escape(data.title) + '</span>' +
                      '</div>';
          },
          item: function(data, escape) {
              return '<div class="item">' + escape(data.title) + '</div>';
          }
      }
    });

    $('.fleet-id-from-dropdown').selectize({
      searchField: 'title',
      valueField: 'value',
      placeholder: 'Pilih satu',
      create: false,
      plugins:['option_click'],
      // items : [1],
      options: [
          {value: 1, title: 'Fleet 1'},
          {value: 2, title: 'Fleet 2'},
          {value: 3, title: 'Fleet 3'},
          {value: 4, title: '+ Add Fleet'}
      ],
      render: {
          option: function(data, escape) {
              return '<div class="option"' +
                      '<span class="title font-14">' + escape(data.title) + '</span>' +
                      '</div>';
          },
          item: function(data, escape) {
              return '<div class="item">' + escape(data.title) + '</div>';
          }
      }
    });

    $('#dropdown-from').change(function() {
        var opval = $(this).val();
        if(opval == 4){
            $('#delete-modal').modal("show");
            // $('.item').addClass("d-none");
            // $('.item')[0].append(`
            //   Pilih Satu
            // `);
        }
    });

    $('.gps-utama-dropdown').selectize({
      searchField: 'title',
      valueField: 'value',
      placeholder: 'Pilih satu',
      // items : [1],
      options: [
          {value: 1, title: 'GPS 1'},
          {value: 2, title: 'GPS 2'},
          {value: 3, title: 'GPS 3'},
          {value: 4, title: 'GPS 4'}
      ],
      render: {
          option: function(data, escape) {
              return '<div class="option"' +
                      '<span class="title font-14">' + escape(data.title) + '</span>' +
                      '</div>';
          },
          item: function(data, escape) {
              return '<div class="item">' + escape(data.title) + '</div>';
          }
      }
    });

    $('.sim-card-dropdown').selectize({
      searchField: 'title',
      valueField: 'value',
      placeholder: 'Pilih satu',
      // items : [1],
      options: [
          {value: 1, title: 'Sim Card 1'},
          {value: 2, title: 'Sim Card 2'},
          {value: 3, title: 'Sim Card 3'},
          {value: 4, title: 'Sim Card 4'}
      ],
      render: {
          option: function(data, escape) {
              return '<div class="option"' +
                      '<span class="title font-14">' + escape(data.title) + '</span>' +
                      '</div>';
          },
          item: function(data, escape) {
              return '<div class="item">' + escape(data.title) + '</div>';
          }
      }
    });

    $('.gps-cadangan-dropdown').selectize({
      searchField: 'title',
      valueField: 'value',
      placeholder: 'Pilih satu',
      // items : [1],
      options: [
          {value: 1, title: 'Product Number 1'},
          {value: 2, title: 'Product Number 2'},
          {value: 3, title: 'Product Number 3'},
          {value: 4, title: 'Product Number 4'}
      ],
      render: {
          option: function(data, escape) {
              return '<div class="option"' +
                      '<span class="title font-14">' + escape(data.title) + '</span>' +
                      '</div>';
          },
          item: function(data, escape) {
              return '<div class="item">' + escape(data.title) + '</div>';
          }
      }
    });

    $('.sim-card-cadangan-dropdown').selectize({
      searchField: 'title',
      valueField: 'value',
      placeholder: 'Pilih satu',
      // items : [1],
      options: [
          {value: 1, title: 'Sim Card 1'},
          {value: 2, title: 'Sim Card 2'},
          {value: 3, title: 'Sim Card 3'},
          {value: 4, title: 'Sim Card 4'}
      ],
      render: {
          option: function(data, escape) {
              return '<div class="option"' +
                      '<span class="title font-14">' + escape(data.title) + '</span>' +
                      '</div>';
          },
          item: function(data, escape) {
              return '<div class="item">' + escape(data.title) + '</div>';
          }
      }
    });

    $(".gps-utama-dropdown, .sim-card-dropdown").on("change", function() {
      // alert($('.dropdown-search').val());
      detorderval();
    });

    function detorderval() {
      var can = false;
      $(".gps-utama-dropdown, .sim-card-dropdown").each(function() {
        console.log($(this).val());
        if ($('.gps-utama-dropdown').val() != '' && $('.sim-card-dropdown').val() != '' ) {
          can = true;
        }
      });
      if (can) {
        $('.detail-order-submit').css({
          background: '#0073E5'
        });
        $('.detail-order-submit').bind('click.doredirect', function() {window.location.href='work-order.html';return false;});
        $('.migration-detail-order-submit').bind('click.migredirect', function() {window.location.href='wo-migration.html';return false;});
        $('.uninstall-detail-order-submit').bind('click.uninredirect', function() {window.location.href='wo-uninstall.html';return false;});
        $('.switch-detail-order-submit').bind('click.swredirect', function() {window.location.href='wo-switch.html';return false;});
        // $('.report-submit').bind('click.poistore', function() {poistore();});
        // $('.report-submit').bind('click.poiredirect', function() {window.location.href='index-report-sub.html';});
      } else {
        $('.detail-order-submit').css({
          background: '#C2C2C2'
        });
        // $('.report-submit').unbind('click.poistore');
        $('.detail-order-submit').unbind('click.doredirect');
        $('.migration-detail-order-submit').unbind('click.migredirect');
        $('.uninstall-detail-order-submit').unbind('click.uninredirect');
        $('.switch-detail-order-submit').unbind('click.swredirect');
      }
    }

    $(".material-description, .material-type, .industry-sector, .material-group").on("change", function() {
      // alert($('.dropdown-search').val());
      infokatalogval();
    });

    function infokatalogval() {
      var can = false;
      $(".material-description, .material-type, .industry-sector, .material-group").each(function() {
        console.log($(this).val());
        if ($('.material-description').val() != '' && $('.material-type').val() != '' && $('.industrial-sector').val() != '' && $('.material-group').val() != '') {
          can = true;
        }
      });
      if (can) {
        $('.katalog-info-submit').css({
          background: '#0073E5'
        });
        $('.katalog-info-submit').bind('click.doredirect', function() {window.location.href='md-katalog-dimensi-form.html';return false;});
      } else {
        $('.katalog-info-submit').css({
          background: '#C2C2C2'
        });
        $('.katalog-info-submit').unbind('click.doredirect');
      }
    }

    $(".gross-weight, .net-weight, .unit-weight, .volume, .volume-unit, .size-dimension, .ean-ups, .unit-iso, .length, .width-input, .height-input").on("change", function() {
      // alert($('.dropdown-search').val());
      dimensikatalogval();
    });

    function dimensikatalogval() {
      var can = false;
      $(".gross-weight, .net-weight, .unit-weight, .volume, .volume-unit, .size-dimension, .ean-ups, .unit-iso, .length, .width-input, .height-input").each(function() {
        console.log($(this).val());
        if ($('.gross-weight').val() != '' && $('.net-weight').val() != '' && $('.unit-weight').val() != '' && $('.volume').val() != '' && $('.volume-unit').val() != '' && $('.size-dimension').val() != '' && $('.ean-ups').val() != '' && $('.unit-iso').val() != '' && $('.length').val() != '' && $('.width-input').val() != '' && $('.height-input').val() != '') {
          can = true;
        }
      });
      if (can) {
        $('.katalog-dimensi-submit').css({
          background: '#0073E5'
        });
        $('.katalog-dimensi-submit').bind('click.doredirect', function() {window.location.href='md-katalog-supplier-form.html';return false;});
      } else {
        $('.katalog-dimensi-submit').css({
          background: '#C2C2C2'
        });
        $('.katalog-dimensi-submit').unbind('click.doredirect');
      }
    }

    $(".nama-vendor, .harga-retail").on("change", function() {
      // alert($('.dropdown-search').val());
      supplierkatalogval();
    });

    function supplierkatalogval() {
      var can = false;
      $(".nama-vendor, .harga-retail").each(function() {
        console.log($(this).val());
        if ($('.nama-vendor').val() != '' && $('.harga-retail').val() != '') {
          can = true;
        }
      });
      if (can) {
        $('.katalog-supplier-submit').css({
          background: '#0073E5'
        });
        $('.katalog-supplier-submit').bind('click.doredirect', function() {sessionStorage.setItem('append', true);window.location.href='md-katalog.html';return false;});
      } else {
        $('.katalog-supplier-submit').css({
          background: '#C2C2C2'
        });
        $('.katalog-supplier-submit').unbind('click.doredirect');
      }
    }

    // $(".selectize-input" ).change(function() {
    //     if ($('.selectize-input').has('.input-active')) {
    //         $('.item').addClass("me-1");
    //     }
    // });


    (function () {
      'use strict'

      // Fetch all the forms we want to apply custom Bootstrap validation styles to
      var forms = document.querySelectorAll('.needs-validation')

      // Loop over them and prevent submission
      Array.prototype.slice.call(forms)
        .forEach(function (form) {
          form.addEventListener('submit', function (event) {
            if (!form.checkValidity()) {
              event.preventDefault()
              event.stopPropagation()
            }

            form.classList.add('was-validated')
          }, false)
        })
    })()

    $('.detail-order-submit').click(function() {
      if ($('.gps-utama-dropdown').val() == '') {
        $('.gps-utama-dropdown .items').addClass("selectize-input-invalid");
        $('.invalid-selectize.gps-utama').removeClass("d-none");
      } else{
        $('.gps-utama-dropdown .items').removeClass("selectize-input-invalid");
        $('.invalid-selectize.gps-utama').addClass("d-none");
      }

      if ($('.fleet-from-dropdown').val() == '') {
        $('.fleet-from-dropdown .items').addClass("selectize-input-invalid");
        $('.invalid-selectize.gps-utama').removeClass("d-none");
      } else{
        $('.fleet-from-dropdown .items').removeClass("selectize-input-invalid");
        $('.invalid-selectize.gps-utama').addClass("d-none");
      }

      if ($('.fleet-id-from-dropdown').val() == '') {
        $('.fleet-id-from-dropdown .items').addClass("selectize-input-invalid");
        $('.invalid-selectize.gps-utama').removeClass("d-none");
      } else{
        $('.fleet-id-from-dropdown .items').removeClass("selectize-input-invalid");
        $('.invalid-selectize.gps-utama').addClass("d-none");
      }

      if ($('.fleet-to-dropdown').val() == '') {
        $('.fleet-to-dropdown .items').addClass("selectize-input-invalid");
        $('.invalid-selectize.gps-utama').removeClass("d-none");
      } else{
        $('.fleet-to-dropdown .items').removeClass("selectize-input-invalid");
        $('.invalid-selectize.gps-utama').addClass("d-none");
      }

      if ($('.fleet-id-to-dropdown').val() == '') {
        $('.fleet-id-to-dropdown .items').addClass("selectize-input-invalid");
        $('.invalid-selectize.gps-utama').removeClass("d-none");
      } else{
        $('.fleet-id-to-dropdown .items').removeClass("selectize-input-invalid");
        $('.invalid-selectize.gps-utama').addClass("d-none");
      }

      if ($('.sim-card-dropdown').val() == '') {
        $('.sim-card-dropdown .items').addClass("selectize-input-invalid");
        $('.invalid-selectize.sim-card').removeClass("d-none");
      } else{
        $('.sim-card-dropdown .items').removeClass("selectize-input-invalid");
        $('.invalid-selectize.sim-card').addClass("d-none");
      }
    });

    $(".fleet-from-dropdown" ).change(function() {
      if ($(this).val() != '') {
          $('.fleet-from-dropdown .items').removeClass("selectize-input-invalid");
          $('.invalid-selectize.gps-utama').addClass("d-none");
      }

      if ($(this).val() == 1 || $(this).val() == 2 || $(this).val() == 3 || $(this).val() == 4) {
        $('.fleet-to-dropdown').data('selectize').setValue(5);
      }else {
        $('.fleet-to-dropdown').data('selectize').setValue();
      }

    });

    $(".fleet-to-dropdown" ).change(function() {
      if ($(this).val() != '') {
          $('.fleet-to-dropdown .items').removeClass("selectize-input-invalid");
          $('.invalid-selectize.gps-utama').addClass("d-none");
      }
    });

    $(".fleet-id-from-dropdown" ).change(function() {
      if ($(this).val() != '') {
          $('.fleet-id-from-dropdown .items').removeClass("selectize-input-invalid");
          $('.invalid-selectize.gps-utama').addClass("d-none");
      }

      if ($(this).val() == 1 || $(this).val() == 2 || $(this).val() == 3 || $(this).val() == 4) {
        $('.fleet-id-to-dropdown').data('selectize').setValue(5);
      }else {
        $('.fleet-id-to-dropdown').data('selectize').setValue();
      }

    });

    $(".fleet-id-to-dropdown" ).change(function() {
      if ($(this).val() != '') {
          $('.fleet-id-to-dropdown .items').removeClass("selectize-input-invalid");
          $('.invalid-selectize.gps-utama').addClass("d-none");
      }
    });

    $(".gps-utama-dropdown" ).change(function() {
      if ($(this).val() != '') {
          $('.gps-utama-dropdown .items').removeClass("selectize-input-invalid");
          $('.invalid-selectize.gps-utama').addClass("d-none");
      }

      if ($(this).val() == 1 || $(this).val() == 2 || $(this).val() == 3 || $(this).val() == 4) {
        $('.mt-value').val("Terisi Otomatis");
        $('.md-value').val("Terisi Otomatis");
        $('.w-value').val("Terisi Otomatis");
        $('.arrow-left-right').removeClass('d-none')
        $('.arrow-left-right-2').removeClass('d-none')
      }else {
        $('.mt-value').val("");
        $('.md-value').val("");
        $('.w-value').val("");
        $('.arrow-left-right').addClass('d-none')
        $('.arrow-left-right-2').addClass('d-none')
      }
    });

    $(".gps-cadangan-dropdown" ).change(function() {
      if ($(this).val() != '') {
          $('.items').removeClass("selectize-input-invalid");
          $('.invalid-selectize').addClass("d-none");
      }

      if ($(this).val() == 1 || $(this).val() == 2 || $(this).val() == 3 || $(this).val() == 4) {
        $('.mtc-value').val("Terisi Otomatis");
        $('.mdc-value').val("Terisi Otomatis");
        $('.wac-value').val("Terisi Otomatis");
      }else {
        $('.mtc-value').val("");
        $('.mdc-value').val("");
        $('.wac-value').val("");
      }
    });
    
    $(".sim-card-dropdown" ).change(function() {
      if ($(this).val() != '') {
          $('.sim-card-dropdown .items').removeClass("selectize-input-invalid");
          $('.invalid-selectize.sim-card').addClass("d-none");
      }

      if ($(this).val() == 1 || $(this).val() == 2 || $(this).val() == 3 || $(this).val() == 4) {
        $('.sc-value').val("Terisi Otomatis");
      }else {
        $('.sc-value').val("");
      }
    });

    $(".sim-card-cadangan-dropdown" ).change(function() {
      if ($(this).val() != '') {
          $('.items').removeClass("selectize-input-invalid");
          $('.invalid-selectize').addClass("d-none");
      }

      if ($(this).val() == 1 || $(this).val() == 2 || $(this).val() == 3 || $(this).val() == 4) {
        $('.scc-value').val("Terisi Otomatis");
      }else {
        $('.scc-value').val("");
      }
    });

});